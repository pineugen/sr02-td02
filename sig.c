#include <stdio.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

void routine_pere();
void routine_fils();
void capt_pere();
void capt_fils();

struct sigaction sigact_pere, sigact_fils;
int SIGINT_fils = 0, SIGINT_pere = 0;
pid_t pid;

main()
{
    pid = fork();
    switch (pid)
    {
    case -1:
        printf("Il y'a d'erreur dans fork()\n");
        break;
    case 0:
        routine_fils();
        break;
    default:
        routine_pere();
        break;
    }
}

void routine_fils()
{
    int i = 0;
    initrec();
    sigact_fils.sa_handler = capt_fils;
    sigaction(SIGINT, &sigact_fils, NULL);
    while (i != -1)
    {
        i = attendreclic();
        if (i == 0) // Fils reçoit un clic dans le bouton _0_
        {
            printf("Fils envoit SIGINT au son père\n");
            kill(getppid(), SIGINT);
            //Ligne rajoutée pour envoyer un signal SIGINT au fils
            kill(getpid(), SIGINT);
        }
        printf("Fils: pid du mon père = %d\n", getppid());
    }
    printf("Fils: fin du fils après clic sur _fin_\n");
    exit(EXIT_SUCCESS);
}

void routine_pere()
{
    int n;
    sigact_pere.sa_handler = capt_pere;
    sigaction(SIGINT, &sigact_pere, NULL);
    while (1)
    {
        n = sleep(10);
        printf("Père %d: Le temps restant = %d\n", getpid(), n);
    }
}

void capt_fils() // Condition à envoyer SIGINT au fils???
{
    SIGINT_fils++;
    printf("Fils %d: signal %d reçu\n", getpid(), SIGINT_fils);
    rectvert(5);
    if (SIGINT_fils == 3)
    {
        printf("Fils: fin du fils, 3 signaux reçu\n", getppid());
        exit(EXIT_SUCCESS);
    }
}

void capt_pere()
{
    SIGINT_pere++;
    printf("Père %d: signal %d reçu\n", getpid(), SIGINT_pere);
    if (SIGINT_pere == 3)
    {
        printf("Père: fin du père, 3 signaux reçu\n");
        wait(NULL); // Attendre le fils à terminer
        exit(EXIT_SUCCESS);
    }
}
